﻿namespace RentATruck.Mantenimientos
{
    partial class MantNCF_Nuevo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MantNCF_Nuevo));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.cmdGenerar = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSecuenciaHasta = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSecuenciaDesde = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbTCF = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.SteelBlue;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.HideSelection = false;
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(882, 38);
            this.textBox1.TabIndex = 88;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "Generar NCF Nuevos";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.Color.SteelBlue;
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.ForeColor = System.Drawing.Color.White;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 24;
            this.listBox1.Location = new System.Drawing.Point(623, 44);
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBox1.Size = new System.Drawing.Size(235, 364);
            this.listBox1.TabIndex = 104;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.SteelBlue;
            this.button2.Enabled = false;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(435, 388);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(182, 38);
            this.button2.TabIndex = 108;
            this.button2.Text = "Guardar NCF";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cmdGenerar
            // 
            this.cmdGenerar.BackColor = System.Drawing.Color.SteelBlue;
            this.cmdGenerar.Enabled = false;
            this.cmdGenerar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdGenerar.ForeColor = System.Drawing.Color.White;
            this.cmdGenerar.Location = new System.Drawing.Point(435, 344);
            this.cmdGenerar.Name = "cmdGenerar";
            this.cmdGenerar.Size = new System.Drawing.Size(182, 38);
            this.cmdGenerar.TabIndex = 107;
            this.cmdGenerar.Text = "Generar Secuencia";
            this.cmdGenerar.UseVisualStyleBackColor = false;
            this.cmdGenerar.Click += new System.EventHandler(this.cmdGenerar_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.SteelBlue;
            this.label7.Location = new System.Drawing.Point(203, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(197, 25);
            this.label7.TabIndex = 114;
            this.label7.Text = "Secuencia Hasta:";
            // 
            // txtSecuenciaHasta
            // 
            this.txtSecuenciaHasta.Enabled = false;
            this.txtSecuenciaHasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecuenciaHasta.Location = new System.Drawing.Point(404, 139);
            this.txtSecuenciaHasta.MaxLength = 8;
            this.txtSecuenciaHasta.Name = "txtSecuenciaHasta";
            this.txtSecuenciaHasta.Size = new System.Drawing.Size(196, 31);
            this.txtSecuenciaHasta.TabIndex = 113;
            this.txtSecuenciaHasta.TextChanged += new System.EventHandler(this.txtSecuenciaHasta_TextChanged);
            this.txtSecuenciaHasta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSecuenciaHasta_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.SteelBlue;
            this.label6.Location = new System.Drawing.Point(199, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(203, 25);
            this.label6.TabIndex = 112;
            this.label6.Text = "Secuencia Desde:";
            // 
            // txtSecuenciaDesde
            // 
            this.txtSecuenciaDesde.Enabled = false;
            this.txtSecuenciaDesde.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecuenciaDesde.Location = new System.Drawing.Point(404, 96);
            this.txtSecuenciaDesde.MaxLength = 8;
            this.txtSecuenciaDesde.Name = "txtSecuenciaDesde";
            this.txtSecuenciaDesde.Size = new System.Drawing.Size(196, 31);
            this.txtSecuenciaDesde.TabIndex = 111;
            this.txtSecuenciaDesde.TextChanged += new System.EventHandler(this.txtSecuenciaDesde_TextChanged);
            this.txtSecuenciaDesde.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSecuenciaDesde_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.SteelBlue;
            this.label5.Location = new System.Drawing.Point(12, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 20);
            this.label5.TabIndex = 110;
            this.label5.Text = "Tipo NCF:";
            // 
            // cmbTCF
            // 
            this.cmbTCF.BackColor = System.Drawing.Color.White;
            this.cmbTCF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTCF.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTCF.ForeColor = System.Drawing.Color.Black;
            this.cmbTCF.FormattingEnabled = true;
            this.cmbTCF.Location = new System.Drawing.Point(106, 57);
            this.cmbTCF.Name = "cmbTCF";
            this.cmbTCF.Size = new System.Drawing.Size(494, 26);
            this.cmbTCF.TabIndex = 109;
            this.cmbTCF.SelectionChangeCommitted += new System.EventHandler(this.cmbTCF_SelectionChangeCommitted);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::RentATruck.Properties.Resources.FacturaCreditoFiscal_03;
            this.pictureBox1.Location = new System.Drawing.Point(0, 176);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(396, 250);
            this.pictureBox1.TabIndex = 115;
            this.pictureBox1.TabStop = false;
            // 
            // MantNCF_Nuevo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(882, 446);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtSecuenciaHasta);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtSecuenciaDesde);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbTCF);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.cmdGenerar);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MantNCF_Nuevo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.MantNCF_Nuevo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button cmdGenerar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSecuenciaHasta;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSecuenciaDesde;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbTCF;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}