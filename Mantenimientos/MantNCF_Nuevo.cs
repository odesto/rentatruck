﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentATruck.Mantenimientos
{
    public partial class MantNCF_Nuevo : Form
    {
        datos obDatos = new datos();
        string total = "";
        private static MantNCF_Nuevo ncfnuevoInstancia = null;

        public MantNCF_Nuevo()
        {
            InitializeComponent();
        }

        private void MantNCF_Nuevo_Load(object sender, EventArgs e)
        {
            cargarComboBox();
        }

        public void cargarComboBox()
        {
            obDatos.Conectar();
            this.cmbTCF.DataSource = obDatos.ConsultaTabla("tipo_NCF", " descri_tncf");
            this.cmbTCF.DisplayMember = "descri_tncf";
            this.cmbTCF.ValueMember = "codigo_tncf";
            obDatos.Desconectar();
        }

        public static MantNCF_Nuevo InstanciaNCF_nuevo()
        {
            if ((ncfnuevoInstancia == null) || (ncfnuevoInstancia.IsDisposed == true))
            {
                ncfnuevoInstancia = new MantNCF_Nuevo();
            }
            ncfnuevoInstancia.BringToFront();
            return ncfnuevoInstancia;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmbTCF_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtSecuenciaDesde.Enabled = true;
        }

        private void txtSecuenciaHasta_TextChanged(object sender, EventArgs e)
        {
            cmdGenerar.Enabled = true;
        }

        private void txtSecuenciaDesde_TextChanged(object sender, EventArgs e)
        {
            txtSecuenciaHasta.Enabled = true;
        }

        private void txtSecuenciaDesde_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Validando que no pueda ingresar espacios, letras y simbolos
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtSecuenciaHasta_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Validando que no pueda ingresar espacios, letras y simbolos
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void cmdGenerar_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(this.txtSecuenciaDesde.Text) < Int32.Parse(this.txtSecuenciaHasta.Text))
            {
                total = 'B' + this.cmbTCF.SelectedValue.ToString() + total;
                string total1, total2, total3, total4, total5, total6, total7, total8 = "";

                int desde = Convert.ToInt32(this.txtSecuenciaDesde.Text);
                int hasta = Convert.ToInt32(this.txtSecuenciaHasta.Text);

                for (int a = desde; a <= hasta; a++)
                {
                    if (a < 10)
                    {
                        total1 = "0000000" + a.ToString();
                        this.listBox1.Items.Add(total + total1.ToString());
                    }
                    else if (a >= 10 && a < 100)
                    {
                        total2 = "000000" + a.ToString();
                        this.listBox1.Items.Add(total + total2.ToString());
                    }
                    else if (a >= 100 && a < 1000)
                    {
                        total3 = "00000" + a.ToString();
                        this.listBox1.Items.Add(total + total3.ToString());
                    }
                    else if (a >= 1000 && a < 10000)
                    {
                        total4 = "0000" + a.ToString();
                        this.listBox1.Items.Add(total + total4.ToString());
                    }
                    else if (a >= 10000 && a < 100000)
                    {
                        total5 = "000" + a.ToString();
                        this.listBox1.Items.Add(total + total5.ToString());
                    }
                    else if (a >= 100000 && a < 1000000)
                    {
                        total6 = "00" + a.ToString();
                        this.listBox1.Items.Add(total + total6.ToString());
                    }
                    else if (a >= 1000000 && a < 10000000)
                    {
                        total7 = "0" + a.ToString();
                        this.listBox1.Items.Add(total + total7.ToString());
                    }
                    else if (a >= 10000000 && a < 100000000)
                    {
                        total8 = "" + a.ToString();
                        this.listBox1.Items.Add(total + total8.ToString());
                    }
                }
                this.cmdGenerar.Enabled = false;
            }
            else
            {
                MessageBox.Show("El numero DESDE no puede ser mayor que HASTA");
            }
            this.button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                obDatos.Conectar();
                for (int i = 0; i < listBox1.Items.Count; i++)
                {
                    //MessageBox.Show(listBox1.Items[i].ToString());
                    string sql = "insert into NCF_nuevo (ncf_ncf,codigo_tncf,estado) values ('" + listBox1.Items[i].ToString() + "','" + this.cmbTCF.SelectedValue.ToString() + "','TRUE')";

                    if (obDatos.Insertar(sql))
                    {
                        //MessageBox.Show("Registro Insertado");
                    }
                    else
                    {
                        //MessageBox.Show("Registro Insertado");
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            MessageBox.Show("NCF Insertados");
            obDatos.Desconectar();

            this.Controls.Clear();
            this.InitializeComponent();
            cargarComboBox();
        }
    }
}
