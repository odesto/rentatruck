﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentATruck.Reportes.NCF_Nuevos
{
    public partial class Form1 : Form
    {
        private static Form1 ncfnuevosActivos = null;

        public Form1()
        {
            InitializeComponent();
        }

        public static Form1 InstanciaReporteNCFActivosNuevos()
        {
            if ((ncfnuevosActivos == null) || (ncfnuevosActivos.IsDisposed == true))
            {
                ncfnuevosActivos = new Form1();
            }
            ncfnuevosActivos.BringToFront();
            return ncfnuevosActivos;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            datos objDatos = new datos();
            objDatos.Conectar();
            objDatos.Consulta_llenar_datos("select * from NCF_nuevoActivos");

            Reportes.NCF_Nuevos.NCNuevosFActivos rp = new Reportes.NCF_Nuevos.NCNuevosFActivos();
            rp.SetDataSource(objDatos.ds.Tables[0]);
            crystalReportViewer1.ReportSource = rp;
            crystalReportViewer1.RefreshReport();
            objDatos.Desconectar();

            crystalReportViewer1.ShowCloseButton = false;
            crystalReportViewer1.ShowCopyButton = false;
            crystalReportViewer1.ShowGotoPageButton = false;
            crystalReportViewer1.ShowLogo = false;
            crystalReportViewer1.ShowParameterPanelButton = false;
            crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            crystalReportViewer1.ShowParameterPanelButton = false;
            crystalReportViewer1.DisplayStatusBar = false;
            this.crystalReportViewer1.RefreshReport();
        }
    }
}
